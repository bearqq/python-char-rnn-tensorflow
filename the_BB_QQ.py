# -*- coding: utf-8 -*-
#python
#https://github.com/bearqq/qqbot
#https://github.com/pandolia/qqbot

import re
import os,sys

try:
    from xmlrpclib import ServerProxy
except:
    from xmlrpc.client import ServerProxy
xmls = ServerProxy('http://localhost:15000')

from qqbot import QQBot

class MyQQBot(QQBot):
    kb=0
    
    def onPollComplete(self, msgType, from_uin, buddy_uin, message):
        
        #/kb
        if message=='[face98]':
            if self.kb==1:
                self.send(msgType, from_uin, [["face",98]])
                self.kb=0
            else:
                self.kb=1
        else:
            self.kb=0
        
        #xmlrpc reply
        match = re.match(r'^(?:!|！)\s?(.+)\s*$', message)
        if match:
            prefix=[["face",98]] if match.group(1).startswith('[face98]') else []
            raw_content = match.group(1).replace('[face98]','')#.decode('utf8')
            if raw_content:
                self.send(msgType, from_uin, prefix+[xmls.reply(raw_content)])

myqqbot = MyQQBot()
myqqbot.Login()
myqqbot.Run()



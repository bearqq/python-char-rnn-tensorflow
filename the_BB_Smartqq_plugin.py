# -*- coding: utf-8 -*-
import re
import os,sys

from smart_qq_bot.logger import logger
from smart_qq_bot.signals import (
    on_all_message,
    on_group_message,
)


from xmlrpc.client import ServerProxy
s = ServerProxy('http://localhost:15000', allow_none=False)

@on_group_message(name='BB_R')
def BB_R(msg, bot):
    #global text
    reply = bot.reply_msg(msg, return_function=True)
    group_code = str(msg.group_code)
    #group_id = str(bot.get_group_info(group_code=group_code).get('id'))    #return error

    match = re.match(r'^(?:!|！)\s?(.+)\s*$', msg.content)
    if match:
        raw_content = match.group(1)#.decode('utf8')
        if raw_content:
            reply(s.reply(raw_content))
            #text=raw_content
            #reply(next(rep))
            return True
    return False


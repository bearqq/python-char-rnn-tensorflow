# coding: utf-8

#some definitions:
#easy=None

#imports
import sys
import threading
import time
from bottle import route, run, template, Bottle, request, error, redirect, abort, static_file

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

application = Bottle()

class easy_plot():
	host=''
	port=8888
	data={}
	len_limit=5000
	
	def _check_len(self,limit=len_limit):
		for k in self.data.keys():
			if len(self.data[k])>=limit:
				#self.data[k]=self.data[k][limit/2:]
				self.data[k]=self.data[k][::2]
	
	def add(self,new_data):
		self._check_len()
		now=time.time()*1000+8*60*60
		for k in new_data.keys():
			if not self.data.get(k):
				self.data[k]=[]
			self.data[k].append([now,new_data[k]])

@application.route('/statics/<path:path>')
def server_static(path):
	return static_file(path, root='./statics')

@application.route('/')
def index():
	return template('index',datas=easy.data)
	
#def easy_init():
	#global easy
easy=easy_plot()

def run_server():
	run(app=application,host=easy.host,port=easy.port)
t=threading.Thread(target=run_server,args=())
t.start()

	

	
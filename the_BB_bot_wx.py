# coding: utf-8
# python2 or python3
#some definitions:

#imports:
from config import token
import time,os

"""
#tensorflow
import numpy as np
import tensorflow as tf
from six.moves import cPickle
from utils import TextLoader
from model import Model
from six import text_type

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

save_dir='save'
n=500
sample=0
stop=1
text=None

with open(os.path.join(save_dir, 'config.pkl'), 'rb') as f:
    saved_args = cPickle.load(f)
with open(os.path.join(save_dir, 'chars_vocab.pkl'), 'rb') as f:
    chars, vocab = cPickle.load(f)

def rep_g():
    with tf.Session() as sess:
        modelx = Model(saved_args, True)
        tf.initialize_all_variables().run()
        saver = tf.train.Saver(tf.all_variables())
        ckpt = tf.train.get_checkpoint_state(save_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
        modelx.save_para(sess, chars, vocab, n, sample, stop)
        
        while 1:
            try:
                r0=modelx.sample_text(text)
                #print(r.encode('utf-8'))
                #yield r
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)
            
            prime=text if text.endswith('\r\n') else text+'\r\n'
            try:
                r=modelx.sample_text(prime)
                #print(r.encode('utf-8'))
                yield r0+'\r\n'+r.split('\r\n')[1]
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)
rep=rep_g()
"""

try:
    from xmlrpclib import ServerProxy
except:
    from xmlrpc.client import ServerProxy
xmls = ServerProxy('http://localhost:15000')

import itchat


@itchat.msg_register(['Text'])
def text_reply(msg):
    #global text
    #text=msg['Text']
    #itchat.send(rep.next(), msg['FromUserName'])
    itchat.send(xmls.reply(msg['Text']), msg['FromUserName'])

itchat.auto_login(enableCmdQR = True,hotReload = True)
itchat.run()








% rebase('adm_base.tpl')

<meta http-equiv="refresh" content="30">

<section class="content-header">
<script src="statics/js/highcharts.js"></script>
<script src="statics/js/highcharts-more.js"></script>
<script src="statics/js/exporting.js"></script>

%import json
%for key in datas.keys():
<div id="chartdiv_{{key}}" ></div><!--style="height: 100%; width: 100%;"-->
<script type='text/javascript'>
	Highcharts.setOptions({  
		global: {  
			useUTC: false  
		}  
	});  

	$(function () {
		var chart;
		$(document).ready(function () {
		chart = new Highcharts.Chart({
			chart: {
				//width: 1000,
				renderTo: 'chartdiv_{{key}}',
				height: 700,
			},
			colors: ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
			credits: {
				enabled: false,
			},
			exporting: {
			},
			global: {
			},
			labels: {
			},
			lang: {
			},
			legend: {
			},
			loading: {
			},
			navigation: {
			},
			pane: {
			},
			plotOptions: {
				line: {
				},
			},
			series: [
				{
				data: {{json.dumps(datas[key])}},
				type: 'line',
				name: '{{key}}',
				visible: 'true',
				},
			],
			subtitle: {
			},
			title: {
			text: 'Trend of {{key}}',
			},
			tooltip: {
			formatter: function() {
				return '<b>' + this.series.name + '</b><br/>' +Highcharts.dateFormat('%Y-%m-%d', this.x) + '<br/>' +Highcharts.numberFormat(this.y);
				}
			},
			xAxis: {
				type:"datetime",
				//tickInterval: 2,
				gridLineDashStyle: 'Solid',
				gridLineWidth: 1,
				dateTimeLabelFormats:
					{
						//second: '%H:%M:%S',
						//minute: '%e. %b %H:%M',
						//hour: '%b/%e %H:%M',
						day: '%e/%b',
						week: '%e. %b',
						month: '%b %y',
						year: '%Y'
					}
			},
			yAxis: {
				gridLineDashStyle: 'Solid',
				gridLineWidth: 1,
				title: {
					//text: '',
				},
			},
		});
	});
	});
	</script>
%end
</section>
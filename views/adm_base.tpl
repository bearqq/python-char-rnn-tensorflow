<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>控制面板</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="statics/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="statics/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="statics/css/AdminLTE.min.css">
    <link rel="stylesheet" href="statics/css/skins/skin-blue.min.css">

    <!-- jQuery 2.1.4 -->
    <script type="text/javascript" src="statics/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script type="text/javascript" src="statics/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="statics/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="statics/js/html5shiv.js"></script>
        <script src="statics/js/respond.min.js"></script>
    <![endif]-->

  </head>

  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>趋势图</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>趋势图</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <!--li class="header">HEADER</li-->
            <!-- Optionally, you can add icons to the links -->
            <li><a href="/"><i class="fa fa-link"></i> <span>首页</span></a></li>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          {{!base}}
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <!--div class="pull-right hidden-xs">
          Anything you want
        </div-->
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="#">酱.xyz</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->
  </body>
</html>

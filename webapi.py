#coding: utf-8
#16-11-04

#find . -type f -name "*.pyc" -delete

#some definitions:


#imports
import sys,os
import time
import re
import json

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

sys.path.insert(0, 'libs')
from bottle import route, run, template, Bottle, request, error, redirect, abort, static_file
try:
    from xmlrpclib import ServerProxy
except:
    from xmlrpc.client import ServerProxy

#some global inits:
application = Bottle()
xmls = ServerProxy('http://192.168.1.212:15000')

def reply(info,key):
	return xmls.reply(info,key)


@application.route('/statics/<path:path>')
def server_static(path):
	return static_file(path, root='./statics')

@application.route('/status')
def status():
	items=[str(i) for i in request.environ.items()]
	r=json.dumps(items)
	return r

@application.route('/')
def index():
	return status()

@application.route('/openapi/api')
def openapi():
	def is_valid_key(key):
		return 1
	key=request.GET.get('key','N')
	if not is_valid_key(key):
		return

	info=request.GET.get('info','')
	if not info.startswith(('!',u'！')):
		return
	return json.dumps({"code":100000,"text":reply(info.decode('utf-8')[1:],key)})

@application.route('<p:re:.*>')
def other(p):
	return status()
	"""
	if ('php' in request.environ.get('bottle.raw_path')) or (request.environ.get('REMOTE_ADDR') in config.blacklist):
		return redirect('http://cachefly.cachefly.net/100mb.test')
	else:
		abort(404)
	"""

@application.error(404)
def error404(error):
	return "Hello world!"

if __name__ == '__main__':
	run(app=application,host='0.0.0.0',port=80)

# coding: utf-8
# python3
#some definitions:

#imports:
from config import token
import time,os

"""
#tensorflow
import numpy as np
import tensorflow as tf
from six.moves import cPickle
from utils import TextLoader
from model import Model
from six import text_type

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

save_dir='save'
n=500
sample=0
stop=1
text=None

with open(os.path.join(save_dir, 'config.pkl'), 'rb') as f:
    saved_args = cPickle.load(f)
with open(os.path.join(save_dir, 'chars_vocab.pkl'), 'rb') as f:
    chars, vocab = cPickle.load(f)

def rep_g():
    with tf.Session() as sess:
        modelx = Model(saved_args, True)
        tf.initialize_all_variables().run()
        saver = tf.train.Saver(tf.all_variables())
        ckpt = tf.train.get_checkpoint_state(save_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
        modelx.save_para(sess, chars, vocab, n, sample, stop)
        
        while 1:
            try:
                r0=modelx.sample_text(text)
                #print(r.encode('utf-8'))
                #yield r
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)
            
            prime=text if text.endswith('\r\n') else text+'\r\n'
            try:
                r=modelx.sample_text(prime)
                #print(r.encode('utf-8'))
                yield r0+'\r\n'+r.split('\r\n')[1]
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)
rep=rep_g()
"""
from xmlrpc.client import ServerProxy
xmls= ServerProxy('http://localhost:15000', allow_none=False)

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Hi!')


def help(bot, update):
    bot.sendMessage(update.message.chat_id, text='Help!')


def echo(bot, update):
    #global text
    #text=update.message.text
    #bot.sendMessage(update.message.chat_id, text=next(rep))xmls
    bot.sendMessage(update.message.chat_id, text=xmls.reply(update.message.text))


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([Filters.text], echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()














"""
#commands available
#https://github.com/eternnoir/pyTelegramBotAPI

@bot.message_handler(func=lambda m: True)
def echo_all(message):
    bot.reply_to(message, message.text)

# Handles all text messages that match the regular expression
@bot.message_handler(regexp="SOME_REGEXP")
def handle_message(message):
    pass
    
"""

